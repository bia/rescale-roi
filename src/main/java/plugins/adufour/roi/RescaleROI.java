package plugins.adufour.roi;

import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.roi.ROI;
import icy.roi.ROI2D;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DLine;
import plugins.kernel.roi.roi2d.ROI2DPolyLine;
import plugins.kernel.roi.roi2d.ROI2DPolygon;
import plugins.kernel.roi.roi2d.ROI2DRectShape;
import plugins.kernel.roi.roi2d.ROI2DShape;

public class RescaleROI extends EzPlug implements ROIBlock
{
    private enum ROIFilter
    {
        ALL, SELECTED, NON_SELECTED
    }
    
    EzVarSequence             input;
    EzVarEnum<ROIFilter>      roiFilter;
    EzVarBoolean              overwrite;
    
    private final EzVarDouble scaleFactor = new EzVarDouble("scale");
    private final VarROIArray inROI       = new VarROIArray("Input ROI");
    private final VarROIArray rescaledROI = new VarROIArray("Rescaled ROI");
    
    @Override
    public void clean()
    {
        rescaledROI.setValue(null);
    }
    
    @Override
    protected void initialize()
    {
        addEzComponent(input = new EzVarSequence("Input sequence"));
        addEzComponent(roiFilter = new EzVarEnum<ROIFilter>("ROI filter", ROIFilter.values()));
        scaleFactor.setValue(2.0);
        addEzComponent(scaleFactor);
        addEzComponent(overwrite = new EzVarBoolean("Overwrite ROI", false));
    }
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input ROI", inROI);
        scaleFactor.setValue(2.0);
        inputMap.add("scale factor", scaleFactor.getVariable());
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output ROI", rescaledROI);
    }
    
    @Override
    public void execute()
    {
        rescaledROI.setValue(null);
        
        double scale = scaleFactor.getValue();
        
        ROI[] rois = inROI.getValue(true);
        if (!isHeadLess())
        {
            List<ROI> list = input.getValue(true).getROIs();
            
            for (int i = 0; i < list.size(); i++)
            {
                ROI roi = list.get(i);
                if (roi.isSelected() && roiFilter.getValue() == ROIFilter.NON_SELECTED)
                    list.remove(i--);
                else if (!roi.isSelected() && roiFilter.getValue() == ROIFilter.SELECTED) list.remove(i--);
            }
            
            rois = list.toArray(new ROI[list.size()]);
        }
        
        for (ROI roi : rois)
        {
            boolean roiOK = true;
            
            ROI out = roi.getCopy();
            out.setName(roi.getName() + " x" + scale);
            
            if (out instanceof ROI2DRectShape || out instanceof ROI2DLine)
            {
                Rectangle2D b2 = ((ROI2D) out).getBounds2D();
                // translate to origin
                double oldX = b2.getCenterX();
                double oldY = b2.getCenterY();
                b2.setFrame(b2.getX() - oldX, b2.getY() - oldY, b2.getWidth(), b2.getHeight());
                // scale
                b2.setFrame(b2.getX() * scale, b2.getY() * scale, b2.getWidth() * scale, b2.getHeight() * scale);
                // translate back to initial position
                b2.setFrame(b2.getX() + oldX, b2.getY() + oldY, b2.getWidth(), b2.getHeight());
                
                ((ROI2D) out).setBounds2D(b2);
            }
            else if (out instanceof ROI2DShape)
            {
                ROI2DShape shape = (ROI2DShape) out;
                
                // determine the mass center
                Point2D.Double oldCenter = new Point2D.Double();
                List<Point2D> pts = shape.getPoints();
                for (Point2D pt : pts)
                {
                    oldCenter.x += pt.getX();
                    oldCenter.y += pt.getY();
                }
                oldCenter.x /= pts.size();
                oldCenter.y /= pts.size();
                
                // scale the ROI via its anchor points
                for (Point2D pt : pts)
                {
                    double x = pt.getX(), y = pt.getY();
                    
                    // translate to the origin
                    x -= oldCenter.x;
                    y -= oldCenter.y;
                    
                    // scale
                    x *= scale;
                    y *= scale;
                    
                    // translate back to the center
                    x += oldCenter.x;
                    y += oldCenter.y;
                    
                    // set the final point location
                    pt.setLocation(x, y);
                }
                
                if (out instanceof ROI2DPolygon)
                {
                    ROI2DPolygon poly = (ROI2DPolygon) out;
                    
                    poly.setPoints(pts);
                }
                else if (out instanceof ROI2DPolyLine)
                {
                    ROI2DPolyLine poly = (ROI2DPolyLine) out;
                    
                    poly.setPoints(pts);
                }
                else try
                {
                    shape.getClass().getMethod("removeAllPoint").invoke(shape);
                    for (Point2D pt : pts)
                        shape.addPoint(pt, true);
                }
                catch (Exception e)
                {
                    roiOK = false;
                    out.setName(roi.getName());
                    if (!isHeadLess()) new FailedAnnounceFrame("Cannot rescale a " + roi.getSimpleClassName());
                }
            }
            else
            {
                roiOK = false;
                out.setName(roi.getName());
                if (!isHeadLess()) new FailedAnnounceFrame("Cannot rescale a " + roi.getSimpleClassName());
            }
            
            if (isHeadLess())
            {
                rescaledROI.add(out);
            }
            else
            {
                if (roiOK)
                {
                    rescaledROI.add(out);
                    if (overwrite.getValue()) input.getValue(true).removeROI(roi);
                    input.getValue(true).addROI(out);
                    out.setSelected(true);
                }
            }
            
        }
    }
}
